/**
  ******************************************************************************
  * @file    system_MDR1986BE4.c
  * @author  Milandr Application Team
  * @version V1.4.0
  * @date    11/06/2014
  * @brief   CMSIS Cortex-M0 Device Peripheral Access Layer System Source File.
  ******************************************************************************
  * <br><br>
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, Milandr SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT
  * OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2014 Milandr</center></h2>
  ******************************************************************************
  * FILE system_MDR1986BE4.c
  */
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_bkp.h"
//#include "disp.h"

/** @addtogroup __CMSIS CMSIS
  * @{
  */

/** @defgroup MDR1986BE4
 *  @{
 */

/** @addtogroup __MDR1986BE4 MDR1986BE4 System
  * @{
  */

/** @addtogroup System_Private_Includes System Private Includes
  * @{
  */

#include "MDR1986BE4.h"
#include "MDR32F9Qx_config.h"

/** @} */ /* End of group System_Private_Includes */

/** @addtogroup __MDR1986BE4_System_Private_Variables MDR1986BE4 System Private Variables
  * @{
  */

/*******************************************************************************
*  Clock Definitions
*******************************************************************************/
  uint32_t SystemCoreClock = (unsigned int)16000000;         /*!< System Clock Frequency (Core Clock)
                                                         *   default value */

/** @} */ /* End of group __MDR1986BE4_System_Private_Variables */

/** @addtogroup __MDR1986BE4_System_Private_Functions MDR1986BE4 System Private Functions
  * @{
  */

/**
  * @brief  Update SystemCoreClock according to Clock Register Values
  * @note   None
  * @param  None
  * @retval None
  */
void SystemCoreClockUpdate (void)
{
  uint32_t cpu_c1_freq, cpu_c2_freq, cpu_c3_freq;
  uint32_t pll_mul;

  /* Compute CPU_CLK frequency */

  /* Determine CPU_C1 frequency */
  if ((MDR_RST_CLK->CPU_CLOCK & (uint32_t)0x00000002) == (uint32_t)0x00000002)
  {
    cpu_c1_freq = HSE_Value;
  }
  else
  {
    cpu_c1_freq = HSI_Value;
  }

  if ((MDR_RST_CLK->CPU_CLOCK & (uint32_t)0x00000001) == (uint32_t)0x00000001)
  {
    cpu_c1_freq /= 2;
  }

  /* Determine CPU_C2 frequency */
  cpu_c2_freq = cpu_c1_freq;

  if ((MDR_RST_CLK->CPU_CLOCK & (uint32_t)0x00000004) == (uint32_t)0x00000004)
  {
    /* Determine CPU PLL output frequency */
    pll_mul = ((MDR_RST_CLK->PLL_CONTROL >> 8) & (uint32_t)0x0F) + 1;
    cpu_c2_freq *= pll_mul;
  }

  /*Select CPU_CLK from HSI, CPU_C3, LSE, LSI cases */
  switch ((MDR_RST_CLK->CPU_CLOCK >> 8) & (uint32_t)0x03)
  {
    uint32_t tmp;
    case 0 :
      /* HSI */
      SystemCoreClock = HSI_Value;
      break;
    case 1 :
      /* CPU_C3 */
      /* Determine CPU_C3 frequency */
      tmp = MDR_RST_CLK->CPU_CLOCK >> 4 & (uint32_t)0x0F;
      if (tmp & (uint32_t)0x8)
      {
        tmp &= (uint32_t)0x7;
        cpu_c3_freq = cpu_c2_freq / ((uint32_t)2 << tmp);
      }
      else
      {
        cpu_c3_freq = cpu_c2_freq;
      }
      SystemCoreClock = cpu_c3_freq;
      break;
    case 2 :
      /* LSE */
      SystemCoreClock = LSE_Value;
      break;
    default : /* case 3 */
      /* LSI */
      SystemCoreClock = LSI_Value;
      break;
  }
}

/**
  * @brief  Setup the microcontroller system
  *         RST clock configuration to the default reset state
  *         Setup SystemCoreClock variable.
  * @note   This function should be used only after reset.
  * @param  None
  * @retval None
  */
/*
void SystemInit (void)
{
  // Reset the RST clock configuration to the default reset state 

  // Reset all clock but RST_CLK & BKP_CLC bits 
  MDR_RST_CLK->PER1_CLOCK   = (uint32_t)0x30;
  MDR_RST_CLK->PER2_CLOCK   = (uint32_t)0x1010;

  // Reset CPU_CLOCK bits 
  MDR_RST_CLK->CPU_CLOCK   &= (uint32_t)0x00000000;

  // Reset PLL_CONTROL bits 
  MDR_RST_CLK->PLL_CONTROL &= (uint32_t)0x00000000;

  // Reset HSEON and HSEBYP bits 
  MDR_RST_CLK->HS_CONTROL  &= (uint32_t)0x00000000;

  // Reset ADC_MCO_CLOCK bits 
  MDR_RST_CLK->ADC_MCO_CLOCK   &= (uint32_t)0x00000000;

 // SystemCoreClockUpdate();
	RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
		RST_CLK_PCLKcmd(RST_CLK_PCLK_BKP,ENABLE);
    // 1. CPU_CLK = HSI clock 
    // 3. CPU_CLK = 7*HSE/2 clock 

    // Enable HSE clock oscillator 
    RST_CLK_HSEconfig(RST_CLK_HSE_ON);
    while (RST_CLK_HSEstatus() != SUCCESS) {;}                    
    
      // Select HSE clock as CPU_PLL input clock source 
      // Set PLL multiplier to 7                        
      RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv1, 2);		//32 MHz
      // Enable CPU_PLL 
      RST_CLK_CPU_PLLcmd(ENABLE);
			RST_CLK_CPU_PLLuse(ENABLE);
			
      while (RST_CLK_CPU_PLLstatus() != SUCCESS) {;}                     
      
        // Set CPU_C3_prescaler to 2 
        RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1);

				RST_CLK_PCLKPer1_C2_CLKSelection(RST_CLK_PER1_C2_CLK_SRC_CPU_C1);		// 16MHz
		
}*/
void SystemInit (void)
{
	RST_CLK_DeInit();
	RST_CLK_HSEconfig(RST_CLK_HSE_ON);
	if (RST_CLK_HSEstatus() == ERROR) while(1);
	RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv1, RST_CLK_CPU_PLLmul4);
	RST_CLK_CPU_PLLcmd(ENABLE);
	if (RST_CLK_CPU_PLLstatus() == ERROR) while(1);
	RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1);
	RST_CLK_CPU_PLLuse(ENABLE);
	RST_CLK_PCLKPer1_C2_CLKSelection(RST_CLK_PER1_C2_CLK_SRC_PLLCPU);
	RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
}


/** @} */ /* End of group __MDR1986BE4_System_Private_Functions */

/** @} */ /* End of group __MDR1986BE4 */

/** @} */ /* End of group MDR1986BE4 */

/** @} */ /* End of group __CMSIS */

/******************* (C) COPYRIGHT 2014 Milandr *********************************
*
* END OF FILE system_MDR1986BE4.c */
