#ifndef __LED_H__
#define __LED_H__

#include "misc.h"

void led_Init(void);
void led_Work(void);
void led_Toggle(unsigned char num);
void led_Write(unsigned char num, bool on_off);

#endif
