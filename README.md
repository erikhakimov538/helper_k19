# K1986BE234QI HELPER - Code examples
## Example 1) Blink LEDs
### Short Description
This code is written for the K1986BE234QI HELPER board. Its capabilities include controlling the five LEDs of the board by pressing one of the two buttons. Pressing the SA5 button turns on one of the five LEDs while turning off the previous one. Pressing the SA4 button turns on the previous LED and turning off the current LED.
### Building a project in Visual Studio Code
To build a file, you must:
1. open the terminal;
2. go to the project directory using the command:
`cd <Project_directory>`;
3. open the code in vscode with the command:
`code .`;
4. check for CMake and CMake Tools extensions (if not, load them in the extensions tab);
5. run the build process through the task palette `CTRL+SHIFT+P` --> Tasks: Run Task --> build or use key combination `CRTL+SHIFT+B`;
6. then in the project directory there will be a folder called build where the firmware (.hex file) will be written.
### Upload firmware
This process requires the firmware loader "1986VE4WSD.exe".

Loading the firmware is done in several steps: 
1. connect the board to the computer and determine the connection port (in my case COM4), which will need to be written in the input field;
2. turn on the board to boot up by holding down the `LOAD` button (without holding down this button when you try to load files there will be a boot error); 
3. In the loader you have to press the button `Browse...`, find and select the firmware file (.hex); 
4. Start the load by pressing the button `Start` and wait until its completion;
5. You can start the board program by rebooting or by pressing the `RUN` button.

## Example 2) Assembler functions
### Short Description
This code is written for the K1986BE234QI HELPER board. This code is a port of a past C program to  Asm. This code completely repeats the functionality and has new features.
### Building a project in Visual Studio Code
The process of building the project is completely similar to Example 1 and has the same software requirements.
### Upload firmware
To check the rewritten functions, the firmware loading process will also be identical to Example 1.

## Example 3) UART and sprintf()
### Short Description
This code is written for the K1986BE234QI HELPER board. This code was written while learning the processor interrupt functions and the sprintf() function. Output the result through the PuTTY application console.
### Building a project in Visual Studio Code
The process of building the project is completely similar to Example 1 and has the same software requirements.
### Upload firmware
To check the rewritten functions, the firmware loading process will also be identical to Example 1.
### Setting up a PuTTY session
1. open program;
2. select the `Session` item from the side list;
3. in the `Serial line` field enter the port to which the card is connected (in my case it is COM4);
4. enter the speed value in the `Speed` field;
5. in the `Connection type` section select `Serial`;
6. press the `Open` button and reload board.