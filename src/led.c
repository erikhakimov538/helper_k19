#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"

#include "itmr.h"

static unsigned int stime1, stime2;
static unsigned int period1 = 1000, period2 = 2000;

void led_Toggle(unsigned char num);
void led_Write(unsigned char num, bool on_off);

void led_Init(void)
{					

	PORT_InitTypeDef PORT_InitStructure;

  RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA | RST_CLK_PCLK_PORTB, ENABLE);

  PORT_InitStructure.PORT_Pin   = (PORT_Pin_6 | PORT_Pin_7 | PORT_Pin_8 | PORT_Pin_9);
  PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
	
	PORT_InitStructure.PORT_PULL_UP = PORT_PULL_UP_ON;
	PORT_InitStructure.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
  PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
  PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
  PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
  PORT_Init(MDR_PORTA, &PORT_InitStructure); 
	
  PORT_InitStructure.PORT_Pin   = (PORT_Pin_3);
	PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
	
	PORT_InitStructure.PORT_PULL_UP = PORT_PULL_UP_ON;
	PORT_InitStructure.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
  PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
  PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
  PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
  PORT_Init(MDR_PORTB, &PORT_InitStructure); 	
	
	stime1 = stime2 = itmr_GetTick();		
}

void led_Work(void)
{			
		static bool f1 = false, f2 = false;		
		int n;
	/*	
		if(itmr_IsTime(stime1, period1)){
			if(f1) f1 = false;
			else f1 = true;
			for(n = 4; n < 7; n++) led_Write(n, f1);
			stime1 = itmr_GetTick();
		}
*/	
	
		if(itmr_IsTime(stime2, period1)){			
			
			if(f2) f2 = false;
			else f2 = true;			
			
			for(n = 5; n < 6; n++) led_Toggle(n);
			stime2 = itmr_GetTick();
		}			
}

void led_Toggle(unsigned char num)
{
	static bool flags[8] = {false, false, false, false, false, false, false, false};
	
	if(num >= 8) return;
	
	if(flags[num]) flags[num] = false;
	else flags[num] = true;
	switch(num){
		case 0:			
			//PORT_WriteBit(MDR_PORTA, PORT_Pin_2, flags[num] ? Bit_SET : Bit_RESET);
			break;
		case 1:
			//PORT_WriteBit(MDR_PORTA, PORT_Pin_4, flags[num] ? Bit_SET : Bit_RESET);				
			break;
		case 2:
			PORT_WriteBit(MDR_PORTB, PORT_Pin_3, flags[num] ? Bit_SET : Bit_RESET);			
			break;
		case 3:		
			PORT_WriteBit(MDR_PORTA, PORT_Pin_7, flags[num] ? Bit_SET : Bit_RESET);			
			break;
		case 4:			
			PORT_WriteBit(MDR_PORTA, PORT_Pin_6, flags[num] ? Bit_SET : Bit_RESET);			
			break;
		case 5:			
			PORT_WriteBit(MDR_PORTA, PORT_Pin_8, flags[num] ? Bit_SET : Bit_RESET);				
			break;		
		case 6:			
			PORT_WriteBit(MDR_PORTA, PORT_Pin_9, flags[num] ? Bit_SET : Bit_RESET);			
			break;
		case 7:			
			//PORT_WriteBit(MDR_PORTB, PORT_Pin_4, flags[num] ? Bit_SET : Bit_RESET);			
			break;			
		default:			
			break;
	}		
}

void led_Write(unsigned char num, bool on_off)
{
	switch(num){
		case 0:
			//PORT_WriteBit(MDR_PORTA, PORT_Pin_2, on_off ? Bit_SET : Bit_RESET);
			break;
		case 1:
			//PORT_WriteBit(MDR_PORTA, PORT_Pin_4, on_off ? Bit_SET : Bit_RESET);
			break;
		case 2:
			PORT_WriteBit(MDR_PORTB, PORT_Pin_3, on_off ? Bit_SET : Bit_RESET);
			break;
		case 3:
			PORT_WriteBit(MDR_PORTA, PORT_Pin_7, on_off ? Bit_SET : Bit_RESET);
			break;
		case 4:
			PORT_WriteBit(MDR_PORTA, PORT_Pin_6, on_off ? Bit_SET : Bit_RESET);
			break;
		case 5:
			PORT_WriteBit(MDR_PORTA, PORT_Pin_8, on_off ? Bit_SET : Bit_RESET);
			break;		
		case 6:
			PORT_WriteBit(MDR_PORTA, PORT_Pin_9, on_off ? Bit_SET : Bit_RESET);
			break;
		case 7:
		//	PORT_WriteBit(MDR_PORTB, PORT_Pin_4, on_off ? Bit_SET : Bit_RESET);
			break;			
		default:			
			break;
	}		
}
