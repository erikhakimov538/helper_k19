#include <string.h>
#include <stdio.h>

#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_uart.h"

#include "cb.h"
#include "uart.h"
#include "itmr.h"
#include "led.h"

extern uint32_t freq;

static TUARTSettings UARTSettings;

static PORT_InitTypeDef PortInit;
static UART_InitTypeDef UART_InitStructure;

#define BUF_SIZE 512
static char InBuf[BUF_SIZE], OutBuf[BUF_SIZE];
static TCB UartCBIn, UartCBOut;
static bool fDirectOut;
static unsigned int stime;

extern void uart_SetSettings(TUARTSettings* settings);

// hardware init
void uart_Init(unsigned int speed, unsigned short bit_cnt, unsigned short stop_bits, unsigned short parity)
{	
RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB | RST_CLK_PCLK_UART1, ENABLE);	
	
	
	PortInit.PORT_PULL_UP = PORT_PULL_UP_OFF;
	PortInit.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
	PortInit.PORT_PD_SHM = PORT_PD_SHM_OFF;
	PortInit.PORT_PD = PORT_PD_DRIVER;
	PortInit.PORT_GFEN = PORT_GFEN_OFF;	//I've tryed this. Nothing's happened
	PortInit.PORT_FUNC = PORT_FUNC_MAIN;
	PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;
	PortInit.PORT_MODE = PORT_MODE_DIGITAL;
	
	PortInit.PORT_OE = PORT_OE_OUT;
	PortInit.PORT_Pin = PORT_Pin_0;
	PORT_Init(MDR_PORTB, &PortInit);
	
	PortInit.PORT_OE = PORT_OE_IN;
	PortInit.PORT_Pin = PORT_Pin_1;
	PORT_Init(MDR_PORTB, &PortInit);
	
	
	UART_BRGInit(MDR_UART1, UART_HCLKdiv2);
	NVIC_SetPriority(UART1_IRQn, 3);
	NVIC_EnableIRQ(UART1_IRQn);
		
	UARTSettings.speed = UART_InitStructure.UART_BaudRate = speed;
	UARTSettings.bit_cnt = UART_InitStructure.UART_WordLength = bit_cnt;
	UARTSettings.stop_bits = UART_InitStructure.UART_StopBits = stop_bits;
	UARTSettings.parity = UART_InitStructure.UART_Parity = parity;
	
	UART_InitStructure.UART_FIFOMode = UART_FIFO_OFF;	//It must be SO!! Dont touch it!!!
	UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_RXE | UART_HardwareFlowControl_TXE;	
	
	UART_Init(MDR_UART1, &UART_InitStructure);	
	UART_ITConfig(MDR_UART1, UART_IT_TX | UART_IT_RX, ENABLE);
	UART_Cmd(MDR_UART1, ENABLE);

	cb_Init(&UartCBIn, InBuf, BUF_SIZE);
	cb_Init(&UartCBOut, OutBuf, BUF_SIZE);
	fDirectOut = true;
	
	stime = itmr_GetTick();
    NVIC_EnableIRQ(UART1_IRQn);

/*
	RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);

	
	PortInit.PORT_PULL_UP = PORT_PULL_UP_OFF;
	PortInit.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
	PortInit.PORT_PD_SHM = PORT_PD_SHM_OFF;
	PortInit.PORT_PD = PORT_PD_DRIVER;
	PortInit.PORT_GFEN = PORT_GFEN_OFF;
	PortInit.PORT_FUNC = PORT_FUNC_MAIN;
	PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;
	PortInit.PORT_MODE = PORT_MODE_DIGITAL;


	PortInit.PORT_OE = PORT_OE_OUT;
	PortInit.PORT_Pin = PORT_Pin_0;
	PORT_Init(MDR_PORTB, &PortInit);

	
	PortInit.PORT_OE = PORT_OE_IN;
	PortInit.PORT_Pin = PORT_Pin_1;
	PORT_Init(MDR_PORTB, &PortInit);


	RST_CLK_PCLKcmd(RST_CLK_PCLK_UART1, ENABLE);

	UART_BRGInit(MDR_UART1, UART_HCLKdiv1 );

	UART_InitStructure.UART_BaudRate = 115000;
	UART_InitStructure.UART_WordLength = UART_WordLength8b;
	UART_InitStructure.UART_StopBits = UART_StopBits1;
	UART_InitStructure.UART_Parity = UART_Parity_No;
	UART_InitStructure.UART_FIFOMode = UART_FIFO_ON;
	UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_RXE
												| UART_HardwareFlowControl_TXE;

	UART_Init(MDR_UART1, &UART_InitStructure);

	UART_Cmd(MDR_UART1, ENABLE);
*/

}

// 
bool uart_SendByte(char byte)
{
	bool res;
  
  NVIC_DisableIRQ(UART1_IRQn);

  if (fDirectOut) {
    fDirectOut = false;    
		UART_SendData(MDR_UART1, byte);			
    res = true;
  }
  else{
    res = cb_AddByte(&UartCBOut, byte);		  		
	}
	
	NVIC_EnableIRQ(UART1_IRQn);
  
	return res;		
}

bool uart_SendStr(char* str)
{
	
	bool res = false;
	while(*str != 0)
		if((res = uart_SendByte(*str++)) == false) break;		
	return res;
}

bool uart_SendBuf(char* buf, unsigned int size)
{	
	unsigned int ind;
	
	for(ind = 0; ind < size; ind++)
		uart_SendByte(buf[ind]);
	return true;
}

bool uart_GetByte(char* pb)
{
	bool res;
	NVIC_DisableIRQ(UART1_IRQn);
	res = cb_GetByte(&UartCBIn, pb);
	NVIC_EnableIRQ(UART1_IRQn);
	return res;
}

void uart_IRQ(void)
{	
	short hw;
	char b;
	if(UART_GetITStatus(MDR_UART1, UART_IT_RX) == SET){
		UART_ClearITPendingBit(MDR_UART1, UART_IT_RX);		
		hw = UART_ReceiveData(MDR_UART1);
		
	//	led_Toggle(6);		
		
		cb_AddByte(&UartCBIn, (char)(hw & 0xFF));
	}
	
	if(UART_GetITStatus(MDR_UART1, UART_IT_TX) == SET){		
		UART_ClearITPendingBit(MDR_UART1, UART_IT_TX);		
		
	//	led_Toggle(5);	

		if(cb_GetByte(&UartCBOut, &b)){			
			hw = b;			
			UART_SendData(MDR_UART1, hw);				
		}
		else{
			fDirectOut = true;				
		}
	}	
}

void uart_GetSettings(TUARTSettings* settings)
{
	memcpy(settings, &UARTSettings, sizeof(TUARTSettings));
}

void uart_SetSettings(TUARTSettings* settings)
{
	memcpy(&UARTSettings, settings, sizeof(TUARTSettings));
}

void uart_Test(void)
{
	if(itmr_IsTime(stime, 3000)){
		//led_Toggle(1);
		uart_SendByte('D');
		stime = itmr_GetTick();
		
	}
}
