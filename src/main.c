#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "MDR32F9Qx_uart.h"
#include "system_MDR1986BE4.h"
#include "MDR32F9Qx_port.h"
#include "uart.h"
#include "MDR32F9Qx_rst_clk.h"

#define USE_PIN_NUM 1 
#define USE_PIN 0
#define USE_PORT 0  

uint32_t freq = 0;
uint32_t int_div = 0;
uint32_t frac_div = 0;

static PORT_InitTypeDef PortInit;

volatile uint32_t counter = 0;

enum States {
    OFF = 0,
    ON = 1
};

typedef struct leds_desc  {
    uint32_t port;
    uint32_t pin;
    uint32_t pin_number;
    enum States state;
} leds_desc_t;

leds_desc_t leds[] = {
    [4] = { .port = MDR_PORTA,
       .pin = PORT_Pin_9,
       .pin_number = 9,
       .state = OFF
    }, 
    [3] = { .port = MDR_PORTA,
       .pin = PORT_Pin_8,
       .pin_number = 8,
       .state = OFF
    },
    [2] = { .port = MDR_PORTA,
       .pin = PORT_Pin_6,
       .pin_number = 6,
       .state = OFF
    }, 
    [1] = { .port = MDR_PORTA,
       .pin = PORT_Pin_7,
       .pin_number = 7,
       .state = OFF
    },
    [0] = { .port = MDR_PORTA,
       .pin = PORT_Pin_10,
       .pin_number = 10,
       .state = OFF
    }
};

typedef struct button_desc  {
    uint32_t port;
    uint32_t pin;
} button_desc_t;

button_desc_t btns[] = {
    [0] = { .port = MDR_PORTB,
       .pin = PORT_Pin_9
    },
    [1] = { .port = MDR_PORTB,
       .pin = PORT_Pin_10
    },
    [2] = { .port = MDR_PORTB,
       .pin = PORT_Pin_8
    }, 
    [3] = { .port = MDR_PORTB,
       .pin = PORT_Pin_3
    },
    [4] = { .port = MDR_PORTB,
       .pin = PORT_Pin_2
    }
};

typedef struct com_desc  {
    char command[64];
    uint32_t size;
    uint32_t type;
} commands_t;

commands_t cmds[] = {
    [0] = { .command = {'t','e','s','t'},
       .size = 4,
       .type = 1
    },
    [1] = { .command = {'l','e','d',' ','o','n'},
       .size = 6,
       .type = 2
    },
    [2] = { .command = {'l','e','d',' ','o','f','f'},
       .size = 7,
       .type = 3
    },
    [3] = { .command = {'l','e','d',' ','o','f','f',' ','a','l','l'},
       .size = 11,
       .type = 4
    },
    [4] = { .command = {'l','e','d',' ','o','n',' ','a','l','l'},
       .size = 10,
       .type = 5
    },
    [5] = { .command = {'s','u','m','m','a'},
       .size = 5,
       .type = 6
    },
    [6] = { .command = {'l', 'e', 'd', ' ', 't', 'o', 'g', 'g', 'l', 'e'},
        .size = 10,
        .type = 7
    },
    [7] = { .command = {'l', 'e', 'd', ' ', 't', 'o', 'g', 'g', 'l', 'e', ' ', 'a', 'l', 'l'},
        .size = 14,
        .type = 8
    },
    [8] = { .command = {'d','e','l','t','a'},
       .size = 5,
       .type = 9
    }
};

extern void delay_asm(uint32_t nCount);
extern void led_on_asm(uint32_t port, uint32_t pin);
extern void led_off_asm(uint32_t port, uint32_t pin);
extern void led_on_asm_mod(uint32_t port, uint32_t pin_number);
extern void led_off_asm_mod(uint32_t port, uint32_t pin_number);
extern void led_off_asm_loop(uint32_t port, uint32_t pin);
extern uint32_t read_input_state(uint32_t port, uint32_t pin);

void Delay(uint32_t nCount)  {
    //  for (; nCount != 0; nCount--);
    delay_asm(nCount);
}

void LEDOn_i(uint32_t index)  {
    //PORT_SetBits(leds[index].port, leds[index].pin);
    //led_on_asm(leds[index].port, leds[index].pin);
    // led_on_asm_mod(leds[index].port, leds[index].pin_number);
#if (USE_PIN_NUM == 1)
    led_on_asm_mod(leds[index].port, leds[index].pin_number);
    leds[index].state = ON;

#elif (USE_PIN == 1)

    led_on_asm(leds[index].port, leds[index].pin);

#elif (USE_PORT == 1)

    PORT_SetBits(leds[index].port, leds[index].pin);

#endif
}

void LEDOff_i(uint32_t index)  {
    //PORT_ResetBits(leds[index].port, leds[index].pin);
    // led_off_asm(leds[index].port, leds[index].pin);
    led_off_asm_mod(leds[index].port, leds[index].pin_number);
    leds[index].state = OFF;
}

void LEDToggle_i(uint32_t index) {
    if (leds[index].state == OFF) {
        LEDOn_i(index);
    }
    else {
        LEDOff_i(index);
    }
}

void LEDToggle_all(void) {
    uint32_t i;
    for (i = 0; i < 5; i++){
        LEDToggle_i(i);
    }
}

void LEDOff_all(void)  {
    uint32_t i;
    for (i = 0; i < 5; i++)  {
        LEDOff_i(i);
    }
}

void LEDOn_all(void)  {
    uint32_t i;
    for (i = 0; i < 5; i++)  {
        LEDOn_i(i);
    }
}

void SysTick_ISR(void) {
    counter++;
}

void BlinkLED_i(uint32_t num, uint32_t del,uint32_t index)  {
    uint32_t cnt;
    for ( cnt = 0; cnt < num; cnt++)  {
        LEDOn_i(index);
        Delay(del);
        LEDOff_i(index);
        Delay(del);
    }
}

void TapButtonFireLed(uint32_t index_button,uint32_t index_button2)  {
    uint8_t button_status;
    uint8_t button_status2;
    static uint32_t state = 1;
    static uint32_t state2 = 1;
    static uint32_t x = 0;

    // button_status = PORT_ReadInputDataBit(btns[index_button].port, btns[index_button].pin);
    // button_status2 = PORT_ReadInputDataBit(btns[index_button2].port, btns[index_button2].pin);
    button_status = read_input_state(btns[index_button].port, btns[index_button].pin);
    button_status2 = read_input_state(btns[index_button2].port, btns[index_button2].pin);

    if (button_status == 0)  {
        if (state != button_status)  { 
            LEDOn_i(x%(sizeof(leds)/sizeof(leds_desc_t)));
            state = button_status;
            ++x;
        }
    } else if (button_status2 == 0)  {
            if (state2 != button_status2)  { 
            LEDOn_i(x%(sizeof(leds)/sizeof(leds_desc_t)));
            state2 = button_status2;
            --x;
        }
    }
    else  {
        LEDOff_all();
        BlinkLED_i(1, 200000, x%(sizeof(leds)/sizeof(leds_desc_t)));
        state = button_status;
        state2 = button_status2;
    }
}

void ButtonGfg (int index)  {
    PortInit.PORT_Pin   = (btns[index].pin);
    PortInit.PORT_OE    = PORT_OE_IN ;
    PortInit.PORT_FUNC  = PORT_FUNC_PORT;
    PortInit.PORT_MODE  = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_OUTPUT_OFF;

    PORT_Init(btns[index].port, &PortInit);
}

void LedGfg (int index)  {
    PortInit.PORT_Pin   = (leds[index].pin);
    PortInit.PORT_OE    = PORT_OE_OUT;
    PortInit.PORT_FUNC  = PORT_FUNC_PORT;
    PortInit.PORT_MODE  = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_SPEED_SLOW;

    PORT_Init(leds[index].port, &PortInit);
}

void Commander(char *buffer)  {
    uint32_t i;
    uint32_t x;
    float a;
    float b;
    bool cmd_success = false;

    for (i =0; i < sizeof(cmds)/sizeof(commands_t); i++)  {
        if (strncmp(buffer, cmds[i].command, cmds[i].size) == 0) {
            switch(cmds[i].type)  {
                case 1 :
                    uart_SendStr("Test OK \r\n");
                    cmd_success = true;
                    break;
                case 2 :
                    if(isdigit(buffer[7])) {
                        sscanf(&buffer[6], "%d", &x);
                        LEDOn_i(x);
                        uart_SendStr("LEDOn_i OK \r\n");
                        cmd_success = true;
                    }
                    break;
                case 3 :
                    if(isdigit(buffer[8])) {
                        sscanf(&buffer[7], "%d", &x);
                        LEDOff_i(x);
                        uart_SendStr("LEDOff_i OK \r\n");
                        cmd_success = true;  
                    }
                    break;
                case 4 :
                    uart_SendStr("LEDOff_all OK \r\n");
                    LEDOff_all();
                    cmd_success = true;
                    break;
                case 5 :
                    uart_SendStr("LEDOn_all OK \r\n");
                    LEDOn_all();
                    cmd_success = true;
                    break;
                case 6 :
                    uart_SendStr("Summa OK \r\n");
                    sscanf(&buffer[5], "%f %f", &a, &b);
                    a = a + b;
                    memset(buffer, 0, 64);
                    snprintf(buffer, 64, "Summa: %f\r\n", a);
                    uart_SendStr(buffer);
                    cmd_success = true;
                    break;
                case 7 :
                    if (isdigit(buffer[12])) {
                        uart_SendStr("LEDToggle_i OK \r\n");
                        sscanf(&buffer[11], "%d", &x);
                        LEDToggle_i(x);
                        cmd_success = true;
                    }
                    break;
                case 8 :
                    uart_SendStr("LEDToggle_i OK \r\n");
                    LEDToggle_all();
                    cmd_success = true;
                    break;
                case 9 :
                    uart_SendStr("Delta OK \r\n");
                    sscanf(&buffer[5], "%f %f", &a, &b);
                    a = a - b;
                    memset(buffer, 0, 64);
                    snprintf(buffer, 64, "Delta: %f\r\n", a);
                    uart_SendStr(buffer);
                    cmd_success = true;
                    break;
                default :
                    uart_SendStr("Unknown command \r\n");
                    break;
            }
            if (cmd_success == true) {
                break;
            }
        }
    }
        if (i == sizeof(cmds)/sizeof(commands_t)) {
            uart_SendStr("Unknown command \r\n");
        }
    memset(buffer,0,64);
}

int main(void)  {

    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTC, ENABLE);

    SysTick_Config(16000000);

    uint32_t i;
    uint32_t letter_num = 0;

    char res;
    char sep [10]=" ";
    char command;
    char letters[] = {0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x0D, 0x0A};
    char buffer[64];

    for (i =0; i < sizeof(leds)/sizeof(leds_desc_t); i++)  {
        LedGfg(i);
    }

    for (i =0; i < sizeof(btns)/sizeof(leds_desc_t); i++)  {
        ButtonGfg(i);
    }

    uart_Init(115200, UART_WordLength8b, UART_StopBits1, UART_Parity_No);

    // SSP_Init(MDR_SSP1_BASE, SSP_ModeMaster, SSP_WordLength8b, SSP_SPO_Low, SSP_SPH_1Edge, SSP_FRF_SPI_Motorola);

    memset(buffer,0,64);
    sprintf(buffer, " freq = %d\n\r integer divider = %d\n\r fractional divider =%d\n\r", freq, int_div, frac_div);
    uart_SendStr(buffer);

    while(1)  {
        // TapButtonFireLed(0, 1);
        // Delay(100);

        if (uart_GetByte(&res) == true) {
            uart_SendByte(res);
            strncat (buffer, &res, 1);
            if (res == '\r')  {
                uart_SendByte('\n');
                Commander(&buffer[0]);
            }
        }
  }
}

#pragma clang diagnostic pop